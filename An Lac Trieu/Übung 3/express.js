"use strict";
exports.__esModule = true;
var express = require("express");
var bodyParser = require("body-parser");
var router = express();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));
router.use("/site", express.static(__dirname + '/client'));
router.listen(8080, function () {
    console.log("Gestartet: http://localhost:8080/site/view.html");
});
var zahl = 8;
router.get("/raten/:zahl", function (req, res) {
    if (zahl < req.params["zahl"]) {
        res.json({ antwort: "Kleiner!" });
    }
    if (zahl > req.params["zahl"]) {
        res.json({ antwort: "Größer!" });
    }
    if (zahl == req.params["zahl"]) {
        res.json({ antwort: "Richtig!" });
    }
    else {
        res.json({ antwort: "Fehler!" });
    }
});
router.get("/set/:newnumber", function (req, res) {
    zahl = req.params["newnumber"];
    res.json({ antwort: zahl });
});
//# sourceMappingURL=express.js.map