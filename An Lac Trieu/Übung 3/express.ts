import {Request, Response} from "express";
import express = require("express");
import bodyParser = require("body-parser");

let router = express();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: true}));
router.use("/site", express.static(__dirname + '/client'));
router.listen(8080, () => {
    console.log("Gestartet: http://localhost:8080/site/view.html")
});

let zahl: number= 8;
router.get("/raten/:zahl", function (req: Request, res: Response) {
    if (zahl < req.params ["zahl"]) {
        res.json({antwort: "Kleiner!"})
    }
    if (zahl > req.params ["zahl"]) {
        res.json({antwort: "Größer!"})
    }
    if (zahl == req.params ["zahl"]) {
        res.json({antwort: "Richtig!"})
    }
    else {
        res.json({antwort: "Fehler!"})
    }
});

router.get("/set/:newnumber", function (req: Request, res: Response) {
    zahl = req.params["newnumber"];
    res.json({ antwort: zahl })

});
