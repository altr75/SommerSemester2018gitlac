$(function () {
    for (let x: number = 0; x <= 100; x++) {
        //    $("#spalte").append("<div>" + x + "</div>");
    }
    $("#raten").click(() => {
        $.ajax({
            url: 'http://localhost:8080/raten/' + $("#zahlenraten").val(),
            type: 'GET',
            success: (data) => {
                $('#result').html(data.antwort)
            },
            error: (jqXHR, Status, error) => {
                alert(error)
            }
        })
    });
    $("#zahlsetzten_button").click(() => {
        $.ajax({
            url: 'http://localhost:8080/set/' + $("#zahlsetzen_input").val(),
            type: 'GET',
            success: (data) => {
                $('#result').html(data.antwort)
            },
            error: (jqXHR, Status, error) => {
                alert(error)
            }
        })
    })
});

