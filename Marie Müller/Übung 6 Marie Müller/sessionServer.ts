/*****************************************************************************
 ***  Import some module from node.js (see: expressjs.com/en/4x/api.html)    *
 *****************************************************************************/
import express    = require ("express");      // needed for routing
import { Request, Response } from "express";  // Request/Response from Express
//--- install with:  npm install express-session @types/express-session -save
import session    = require('express-session');

/*****************************************************************************
 ***  Create server with handler function and start it                       *
 *****************************************************************************/
let router = express();
router.listen(8080, "localhost", function () {
  console.log(`
    -------------------------------------------------------------
    Ein einfacher session-manager ");
    Dokumentation mit: im Terminal von webStorm");
            apidoc -o apidoc -e node_modules' ");
            starte lokal apidoc/index.html ");
    Öffne: 'sessionTest.http'
    Front-End: http://localhost:8080/site/client.html
    -------------------------------------------------------------
  `);
});

/*****************************************************************************
 ***  Middleware routers                                                     *
 ****************************************************************************/

router.use("/views",   express.static(__dirname + "/views"));
router.use("/css",     express.static(__dirname + "/css"));
router.use("/site",  express.static(__dirname + "/site"));
router.use("/jquery",  express.static(__dirname + "/node_modules/jquery/dist"));

//--- session management -----------------------------------------------------
router.use( session( { //--- session management ------------------------------
	resave            : true,              // save session even if not modified
	saveUninitialized : true,              // save session even if not used
	secret            : "geheim",          // encrypt session-id in cookie
	name              : "mySessionCookie", // name of the cookie name
	cookie            : { maxAge: 20 * 60 * 1000 } // 20 minutes
} ) );


/*****************************************************************************
 ***  routers to handle sessions                                             *
 ***  Note: all routers uses method GET and Parameters via URL to allow      *
 ***  testing in Browser                                                     *
 *****************************************************************************/

/**
 * --- create new session-variable : post /variable/:key/:value ---------------
 * @api        {post} /variable/:key/:value
 * @apiName    Set session-variable as new key-value pair
 * @apiGroup   Variable
 * @apiDescription
 * Sets a new key-value pair in session<
 * @apiParam {number} :key  :key is the key of the key-value
 * @apiParam {number} :value  :value is the value of the key-value
 * @apiExample {url} Usage Example
 * http://localhost:8080/variable/summme/25
 *
 * @apiSuccess (Success 200) {json[]}  session-object
 * @apiSuccessExample {json} 200 (Ok) Example
 * if called and nothing has been changed since last call:
 * HTTP/1.1 200 Ok
 *  {
 *      "cookie": {
 *         "originalMaxAge": null,
 *         "expires": null,
 *         "httpOnly": true,
 *         "path": "/"
 *      },
 *      "summe": "25"
 *  }
 */
router.post("/variable/:key/:value", function (req: Request, res: Response) {
  // Access via request object. Access via e.g. 'req.session.loggedin = true'.
  if (req.params.value == "null") { // "softreset" value of key
    req.session[req.params.key] = null;
  } else {
    req.session[req.params.key] = req.params.value;
  }
  res.json(req.session);
});

/**
 * --- delete session-variable: delete /variable/:key -------------------------
 * @api        {delete} /variable/:key
 * @apiName    removes key-value pair of :key
 * @apiGroup   Variable
 * @apiDescription
 * Removes key-value pair of :key by setting it to "undefined"
 * @apiParam {number} :key  :key is the key of the key-value
 * @apiExample {url} Usage Example
 * http://localhost:8080//variable/summe
 *
 * @apiSuccess (Success 200) {json[]}  session-object
 * @apiSuccessExample {json} 200 (Ok) Example
 * if key is still existing:
 * HTTP/1.1 200 Ok
 *  {
 *      "cookie": {
 *         "originalMaxAge": null,
 *         "expires": null,
 *         "httpOnly": true,
 *         "path": "/"
 *      },
 *  }
 */
router.delete("/variable/:key", function (req: Request, res: Response) {
  req.session[req.params.key] = undefined; // set 'undefined' to fully remove key.
  res.json(req.session);
});

/**
 * --- read session: get /session --------------------------------------
 * @api        {get} /session
 * @apiName    reads session-object
 * @apiGroup   Session
 * @apiDescription
 * Read session object
 * @apiExample {url} Usage ExamplDeletee
 * http://localhost:8080/session
 *
 * @apiSuccess (Success 200) {json[]}  session-object
 * @apiSuccessExample {json} 200 (Ok) Example
 * if called and something has been changed since last call:
 * HTTP/1.1 200 Ok
 *  {
 *      "cookie": {
 *          "originalMaxAge": null,
 *          "expires": null,
 *          "httpOnly": true,
 *          "path": "/"
 *      }
 *  }
 */
router.get("/session", function (req: Request, res: Response) {
	res.json(req.session); // Contains cookie information and session objects.
});

/**
 * --- delete session: delete /session ----------------------------------------
 * @api        {delete} /session
 * @apiName    delete session-object
 * @apiGroup   Session
 * @apiDescription
 * Deletes session object
 * @apiExample {url} Usage Example
 * http://localhost:8080/session
 *
 * @apiSuccess (Success 200) {json[]}  session-object
 * @apiSuccessExample {json} 200 (Ok) Example
 * if called and something has been changed since last call:
 * HTTP/1.1 200 Ok
 * {
 *   "message": "session has been deleted"
 * }
 */
router.delete("/session", function (req: Request, res: Response) {
	req.session.destroy( function () { console.log("session has been deleted"); });
	res.json({"message":"session has been deleted"});
});

/**
 * --- set session expiration time offset : post /maxage/:sec -----------------
 * @api           {post} /maxage/:sec
 * @apiName        Set session expiration time
 * @apiGroup       MaxAge
 * @apiDescription Set session expiration time
 * @apiExample {url} Usage Example
 * http://localhost:8080//maxage/120
 * @apiParam {number} :sec  :sec expiration duration (in seconds)
 *
 * @apiSuccess (Success 200) {json[]}  session-object
 * @apiSuccessExample {json} 200 (Ok) Example
 * if called and nothing has been changed since last call:
 * HTTP/1.1 200 Ok
 * {
 *      "cookie": {
 *          "originalMaxAge": 30000,
 *          "expires": "2017-11-09T18:37:36.171Z",
 *          "httpOnly": true,
 *          "path": "/"
 *      }
 *  }
 */
router.post("/maxage/:sec", function (req: Request, res: Response) {
  req.session.cookie.maxAge = req.params.sec * 1000;
  res.json(req.session);
});

/**
 * --- delete session expiration time offeset: delete /maxage -----------------
 * @api        {delete} /maxage
 * @apiName    Reset session expiration time
 * @apiGroup   MaxAge
 * @apiDescription
 * Reset session expiration time<br />
 * @apiExample {url} Usage Example
 * http://localhost:8080/maxage
 *
 * @apiSuccess (Success 200) {json[]}  session-object
 * @apiSuccessExample {json} 200 (Ok) Example
 *  {
 *      "cookie": {
 *         "originalMaxAge": null,
 *         "expires": null,
 *         "httpOnly": true,
 *         "path": "/"
 *      },
 *  }
 */
router.delete("/maxage", function (req: Request, res: Response) {
  req.session.cookie.maxAge = null;
  res.json(req.session);
});