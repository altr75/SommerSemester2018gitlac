"use strict";
exports.__esModule = true;
var express = require("express");
var bodyParser = require("body-parser");
var router = express();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));
router.listen(8080, function () {
    console.log("Gestartet: http://localhost:8080/raten/5");
});
router.get("/user/:id", function (req, res) {
    var identifier = req.params["id"];
    res.json({ zahl: identifier });
});
router.post("/article", function (req, res) {
    var name = req.body["title"];
    res.json({ title: name });
});
//# sourceMappingURL=express.js.map