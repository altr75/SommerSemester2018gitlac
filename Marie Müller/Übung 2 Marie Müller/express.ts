import {Request, Response} from "express";
import express = require("express");
import bodyParser = require("body-parser");

let router = express();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: true}));
router.listen(8080, () => {
  console.log("Gestartet: http://localhost:8080/raten/5");
});

router.get("/user/:id", function(req: Request, res:Response) {
  let identifier = req.params["id"];
  res.json({zahl: identifier})
});

router.post("/article", function(req: Request, res: Response) {
  let name = req.body["title"];
  res.json({title: name});
});