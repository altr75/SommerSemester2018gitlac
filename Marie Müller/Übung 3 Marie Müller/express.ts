import {Request, Response} from "express";
import express = require("express");
import bodyParser = require("body-parser");

let router = express();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: true}));
router.listen(8080, () => {
  console.log("Gestartet: http://localhost:8080/site/view.html");
});

router.use("/site", express.static(__dirname + '/client'));
router.use('/jquery', express.static(__dirname + '/node_modules/jquery/dist'));