"use strict";
exports.__esModule = true;
/*****************************************************************************
 ***  Import some module from node.js (see: expressjs.com/en/4x/api.html)    *
 *****************************************************************************/
var express = require("express"); // import EXPRESS
/*****************************************************************************
 ***  user class and array of users                                          *
 *****************************************************************************/
//--- class describing a user -------------------------------------------------
var User = /** @class */ (function () {
    // methods
    function User(id, vname, nname, uname, pwort) {
        this.id = id;
        this.vorname = vname;
        this.nachname = nname;
        this.username = uname;
        this.passwort = pwort;
        // rearrange ISOString defined in [RFC 3339] e.g. 2017-12-31T23:59:6
        // by substituting substrings defined by  regular expression (->Expert;-)
        // this.time     = new Date().toISOString().replace(/(.*)T(.*)\..*Z/, "$1, $2");
        // should work, but needes special node.js installation:
        this.time = new Date().toLocaleString('de-DE');
    }
    return User;
}());
//--- array of all users ------------------------------------------------------
var userList = [];
/*****************************************************************************
 ***  Create server with handler function and start it                       *
 *****************************************************************************/
var router = express();
router.listen(8080, "localhost", function () {
    console.log("");
    console.log("-------------------------------------------------------------");
    console.log("  Client-Server usermanager");
    console.log("  Dokumentation mit: im Terminal von webStorm");
    console.log("                     'apidoc -o apidoc -e node_modules' ");
    console.log("                     starte lokal apidoc/index.html ");
    console.log("");
    console.log("  Aufruf (Zwei Client-Varianten):");
    console.log("  - AJAX:      http://localhost:8080/clientAJAX.html");
    console.log("  - BOOTSTRAP: http://localhost:8080/clientBS.html");
    console.log("-------------------------------------------------------------");
});
/*****************************************************************************
 ***  Static routers                                                         *
 *****************************************************************************/
var baseDir = __dirname + '/../..'; // get rid of /server/src
router.use("/", express.static(baseDir + "/client/views"));
router.use("/css", express.static(baseDir + "/client/css"));
router.use("/src", express.static(baseDir + "/client/src"));
router.use("/jquery", express.static(baseDir + "/client/node_modules/jquery/dist"));
router.use("/popper.js", express.static(baseDir + "/client/node_modules/popper.js/dist"));
router.use("/bootstrap", express.static(baseDir + "/client/node_modules/bootstrap/dist"));
router.use("/font-awesome", express.static(baseDir + "/client/node_modules/font-awesome"));
//--- parsing json -----------------------------------------------------------
router.use(express.json());
/*****************************************************************************
 ***  Dynamic Routers                                                        *
 *****************************************************************************/
/**
 * --- common api-description: 400 BadRequest --------------------------
 * @apiDefine BadRequest
 * @apiError (Error 400) {string} message  description of the error
 * @apiError (Error 400) {json[]} userList List of users: [{"vorname":string, "nachname":string}, ...]
 * @apiErrorExample 400 (Bad Request) Parameter not provided
 * HTTP/1.1 400 Bad Request
 * {
 *   "message"  : "vorname or nachname not provided",
 *   "userList" : [
 *     {"vorname":"Max",   "nachname":"Mustermann"},
 *   ]
 * }
 * @apiErrorExample 400 (Bad Request) wrong Parameter format
 * HTTP/1.1 400 Bad Request
 * {
 *   "message"  : "Id 'Hans' not a number",
 *   "userList" : [
 *     {"vorname":"Max",   "nachname":"Mustermann"},
 *   ]
 * }
 * @apiErrorExample 400 (Bad Request) ID not provided
 * HTTP/1.1 400 Bad Request
 * {
 *   "message"  : "Parameter ID not provided",
 *   "userList" : [
 *     {"id":0, "vorname":"Max", "nachname":"Mustermann"},
 *   ]
 * }
 */
/**
 * --- common api-description: 404 NotFound ----------------------------
 * @apiDefine NotFound
 * @apiError (Error 404) {string} message  description of the error
 * @apiError (Error 404) {json[]} userList List of users: [{"vorname":string, "nachname":string}, ...]
 * @apiErrorExample 404 (Not Found) Example
 * HTTP/1.1 404 Not Found
 * {
 *   "message"  : "Id 42 out of index range",
 *   "userList" : [
 *     {"vorname":"Max",   "nachname":"Mustermann"},
 *     {"vorname":"Sabine","nachname":"Musterfrau"}
 *   ]
 * }
 *
 */
/**
 * --- common api-description: 410 Gone --------------------------------
 * @apiDefine Gone
 * @apiError (Error 410) {string}   message  description of the error
 * @apiError (Error 410) {object[]} userList List of users: [{"vorname":string, "nachname":string}, ...]
 * @apiErrorExample 410 (Gone) Example
 * HTTP/1.1 410 Gone
 * {
 *   "message"  : "User with id 2 already deleted",
 *   "userList" : [
 *     {"vorname":"Max",   "nachname":"Mustermann"},
 *     {"vorname":"Sabine","nachname":"Musterfrau"}
 *   ]
 * }
 */
/**
 * --- create new user with: post /user --------------------------------
 * @api        {post} /user Create new user
 * @apiVersion 1.0.0
 * @apiName    CreateUser
 * @apiGroup   User
 * @apiDescription
 * This route creates a new user with provided parameters and returns <br />
 * - a message with the attributes of the newly created user
 * - a userList containing all users
 * - a user-Object with all attributes of the created user<br/><br/>
 * @apiExample {url} Usage Example
 * http://localhost:8080/user
 *
 * @apiParam {string} vorname  surname of the user
 * @apiParam {string} nachname lastname of the user
 * @apiParamExample {json} Parameters Example
 * vorname=Max&nachname=Mustermann
 *
 * @apiSuccess (Success 201) {string}  message  attributes of newly created user
 * @apiSuccess (Success 201) {json[]}  userList List of users: [{"vorname":string, "nachname":string, ...}, ...]
 * @apiSuccess (Success 201) {Object}  user that have been created: {"vorname":string, "nachname":string, ...}
 * @apiSuccessExample {json} 201 (Created) Example
 * HTTP/1.1 201 Created
 * { "message"  : "Sabine Musterfrau successfully added",
 *   "userList" : [ {"vorname":"Max",   "nachname":"Mustermann", "time":"23.02.2017 15:27:00"},
 *                  {"vorname":"Sabine","nachname":"Musterfrau", "time":"23.02.2017 15:28:00"} ], }
 *
 * @apiUse BadRequest
 */
router.post("/user", function (req, res) {
    var vorname = (req.body.vorname ? req.body.vorname : "").trim();
    var nachname = (req.body.nachname ? req.body.nachname : "").trim();
    var username = (req.body.username ? req.body.username : "").trim();
    var passwort = (req.body.passwort ? req.body.passwort : "").trim();
    var message = "";
    //--- process parameters ----------------------------------------------------
    if ((vorname !== "") && (nachname !== "")) {
        userList.push(new User(userList.length, vorname, nachname)); // add a new User
        message = vorname + " " + nachname + " successfully added";
        res.status(201); // set http status to "Created"
    }
    else {
        res.status(400); // set http status to "Bad Request"
        message = "vorname or nachname not provided";
    }
    //--- prepare and send response ---------------------------------------------
    res.json({ "message": message, "userList": userList });
});
/**
 * --- get user with /user/:id -----------------------------------------
 * @api        {get} /user/:id Read user information
 * @apiVersion 1.0.0
 * @apiName    ReadUser
 * @apiGroup   User
 *
 * @apiParam {number} :id  URL-parameter: <code>id</code> of the user to be read
 *
 * @apiDescription
 * This route reads the attributes of a user with provided <code>id</code> and returns <br />
 * - a message with the attributes of user with id <code>id</code><br />
 * - an userList containing all users
 * - a user-Object with all attributes of the read user<br/><br/>
 *
 * @apiExample {url} Usage Example
 * http://localhost:8080/user/0
 *
 * @apiSuccess (Success 200) {string}  message  attributes of user with id <code>id</code>
 * @apiSuccess (Success 200) {json[]}  userList List of users: [{"vorname":string, "nachname":string, ...}, ...]
 * @apiSuccess (Success 200) {Object}  user that have been read: {"vorname":string, "nachname":string, ...}
 * @apiSuccessExample {json} 200 (ok) Example
 * HTTP/1.1 200 ok
 * { "message"  : "Selected item is Max Mustermann",
 *   "userList" : [ {"vorname":"Max",   "nachname":"Mustermann", "time":"23.02.2017  15:27:00"},
 *                  {"vorname":"Sabine","nachname":"Musterfrau", "time":"23.02.2017  15:28:00"} ],
 *   "user"     : { {"vorname":"Max",   "nachname":"Mustermann", "time":"23.02.2017  15:27:00"} } }
 *
 * @apiUse BadRequest
 * @apiUse NotFound
 * @apiUse Gone
 */
router.get("/user/:id", function (req, res) {
    var id = req.params.id;
    var message = "";
    var user;
    //--- process parameters ----------------------------------------------------
    if (!isNaN(id) && id >= 0 && id < userList.length && userList[id] != null) {
        message = "Selected item is " + userList[id].vorname + " " + userList[id].nachname;
        user = userList[id];
        res.status(200); // set http status to "OK"
    }
    else if (isNaN(id)) {
        message = "Id '" + id + "' not a number";
        res.status(400); // set http status to "Bad Request"
    }
    else if (id < 0 || id >= userList.length) {
        message = "Id " + id + " out of index range";
        res.status(404); // set http status to "Not Found"
    }
    else {
        message = "User with Id " + id + " already deleted";
        res.status(410); // set http status to "Not Found"
    }
    //--- prepare and send response ---------------------------------------------
    res.json({ "message": message, "userList": userList, "user": user });
});
/**
 * --- update user with: put /user/:id ---------------------------------
 * @api        {put} /user/:id Update user
 * @apiVersion 1.0.0
 * @apiName    UpdateUser
 * @apiGroup   User
 * @apiDescription
 * This route changes attributes of a user with provided <code>id</code><br />
 * Only the provided (optional) parameters are hanged.
 * "Update User" returns <br />
 * - a message with the attributes of the updated user
 * - a userList containing all users
 * - a user-Object with all attributes of the updated user<br/><br/>
 *
 * @apiExample {url} Usage Example
 * http://localhost:8080/user/1
 *
 * @apiParam {number} :id  URL-parameter: <code>id</code> of the user to be updated
 * @apiParam {string} [vorname]  surname of the users
 * @apiParam {string} [nachname] lastname of the user
 *
 * @apiParamExample {urlencoded} Parameters Example
 * vorname=Max&nachname=Mustermann
 *
 * @apiSuccess (Success 200) {string}  message  attributes of newly created user
 * @apiSuccess (Success 200) {json[]}  userList List of users: [{"vorname":string, "nachname":string, ...}, ...]
 * @apiSuccess (Success 200) {Object}  user the user-data after update: {"vorname":string, "nachname":string, ...}
 * @apiSuccessExample {json} 201 (Created) Example
 * HTTP/1.1 200 Ok
 * { "message"  : "Updated item is Sabine Mustermann",
 *   "userList" : [ {"vorname":"Max",   "nachname":"Mustermann", "time":"23.02.2017  15:27:00"},
 *                  {"vorname":"Sabine","nachname":"Mustermann", "time":"23.02.2017  15:28:00"} ],
 *   "user"     : { {"vorname":"Sabine","nachname":"Mustermann", "time":"23.02.2017  15:28:00"} } }
 *
 * @apiUse BadRequest
 * @apiUse NotFound
 * @apiUse Gone
 */
router.put("/user/:id", function (req, res) {
    var vorname = (req.body.vorname ? req.body.vorname : "").trim();
    var nachname = (req.body.nachname ? req.body.nachname : "").trim();
    var username = (req.body.username ? req.body.username : "").trim();
    var passwort = (req.body.passwort ? req.body.passwort : "").trim();
    var id = req.params.id;
    var message = "";
    //--- process parameters ----------------------------------------------------
    if (!isNaN(id) && id >= 0 && id < userList.length && userList[id] != null) {
        if (vorname !== "") {
            userList[id].vorname = vorname;
        }
        if (nachname !== "") {
            userList[id].nachname = nachname;
        }
        if (username !== "") {
            userList[id].username = username;
        }
        if (passwort !== "") {
            userList[id].passwort = passwort;
        }
        message = "Updated item is " + userList[id].vorname + " " + userList[id].nachname;
        res.status(200); // set http status to "Ok"
    }
    else if (isNaN(id)) {
        message = "Id '" + id + "' not a number";
        res.status(400); // set http status to "Bad Request"
    }
    else if (id >= userList.length) {
        message = "Id " + id + " out of index range";
        res.status(404); // set http status to "Not Found"
    }
    else {
        message = "User with Id " + id + " already deleted";
        res.status(410); // set http status to "Not Found"
    }
    //--- prepare and send response ---------------------------------------------
    res.json({ "message": message, "userList": userList });
});
/**
 * --- delete user with /user/:id --------------------------------------
 * @api        {delete} /user/:id Delete user
 * @apiVersion 1.0.0
 * @apiName    DeleteUser
 * @apiGroup   User
 *
 * @apiDescription
 * This route deletes a user with provided <code>id</code> and returns <br />
 * - a message with the attributes of user with id <code>id</code><br />
 * - a userList containing all users<br/><br/>
 *
 * @apiExample {url} Usage Example
 * http://localhost:8080/user/0
 *
 * @apiParam {number} :id  URL-parameter: <code>id</code> of the user to be deleted
 *
 * @apiSuccess (Success 200) {string}  message  attributes of user with id <code>id</code>
 * @apiSuccess (Success 200) {json[]}  userList List of users: [{"vorname":string, "nachname":string}, ...]
 * @apiSuccessExample {json} 200 (ok) Example
 * HTTP/1.1 200 ok
 * { "message"  : "Max Mustermann has been deleted",
 *   "userList" : [ {"vorname":"Sabine","nachname":"Musterfrau"} ] }
 *
 * @apiUse BadRequest
 * @apiUse NotFound
 * @apiUse Gone
 */
router["delete"]("/user/:id", function (req, res) {
    var id = req.params.id;
    var message = "";
    //--- process parameters ----------------------------------------------------
    if (!isNaN(id) && id >= 0 && id < userList.length && userList[id] != null) {
        message = userList[id].vorname + " " + userList[id].nachname + " has been deleted";
        userList[id] = null; // Overwrite the user information to null
        res.status(200); // set http status to "OK"
    }
    else if (isNaN(id)) {
        message = "Id '" + id + "' not a number";
        res.status(400); // set http status to "Bad Request"
    }
    else if (id < 0 || id >= userList.length) {
        message = "Id " + id + " out of index range";
        res.status(404); // set http status to "Not Found"
    }
    else {
        message = "User with Id " + id + " already deleted";
        res.status(410); // set http status to "Not Found"
    }
    //--- prepare and send response ---------------------------------------------
    res.json({ "message": message, "userList": userList });
});
/**
 * --- corrupted REST-API command: all /user (ID missing) ---------------------
 * @api        {all} /user corrupted: ID missing
 * @apiVersion 1.0.0
 * @apiName    WrongAPIcall
 * @apiGroup   User
 * @apiDescription
 * This routes checks for API-call with no ID provided
 *
 * @apiExample {url} Usage Example
 * http://localhost:8080/user
 *
 * @apiUse BadRequest
 */
router.all("/user", function (req, res) {
    res.status(400);
    res.json({ "message": "Parameter ID not provided", "userList": userList });
});
/**
 * --- read user list with: get /users ---------------------------------
 * @api        {get} /users Read list of users
 * @apiVersion 1.0.0
 * @apiName    readUsers
 * @apiGroup   Users
 * @apiDescription
 * This route returns <br />
 * - a message with the numbers of users in list
 * - a userList containing all users - possibly empty<br/><br/>
 * @apiExample {url} Usage Example
 * http://localhost:8080/users
 *
 * @apiSuccess (Success 200) {string}  message  numbers of users in list
 * @apiSuccess (Success 200) {json[]}  userList List of users: [{"vorname":string, "nachname":string}, ...]
 * @apiSuccessExample {json} 200 (Ok) Example
 * HTTP/1.1 200 Ok
 * { "message"  : "There are 2 users in list",
 *   "userList" : [ {"vorname":"Max",   "nachname":"Mustermann"},
 *                  {"vorname":"Sabine","nachname":"Musterfrau"} ] }
 */
router.get("/users", function (req, res) {
    var message;
    var id = userList.length;
    var noElements = 0;
    //--- construct message ----------------------------------------------------
    while (id--) { // iterate through list, starting at the end and decrementing
        if (userList[id] != null)
            noElements++; // count not deleted elements
    } // remark: loop stops with id = -1
    message = "There are " + noElements + " users in list";
    res.status(200); // set http status to "Ok"
    //--- prepare and send response ---------------------------------------------
    res.json({ "message": message, "userList": userList }); // return message
});
/**
 * --- delete user list with: delete /users ----------------------------
 * @api        {delete} /users Delete list of users
 * @apiVersion 1.0.0
 * @apiName    DeleteUsers
 * @apiGroup   Users
 * @apiDescription
 * This route deletes the list of users and return <br />
 * - a message with the numbers of users in list that have been deleted
 * - a userList containing all users - possibly empty<br/><br/>
 * @apiExample {url} Usage Example
 * http://localhost:8080/users
 *
 * @apiSuccess (Success 200) {string}  message  numbers of users in list that have been deleted
 * @apiSuccess (Success 200) {json[]}  userList empty list of users: []
 * @apiSuccessExample {json} 200 (Ok) Example
 * HTTP/1.1 200 Ok
 * { "message"  : "2 users have been deleted",
 *   "userList" : []                            }
 */
router["delete"]("/users", function (req, res) {
    var message;
    var id = userList.length;
    var noElements = 0;
    //--- construct message ----------------------------------------------------
    while (id--) { // iterate through list, starting at the end and decrementing
        if (userList[id] != null)
            noElements++; // count not deleted elements
    } // remark: loop stops with id = -1
    message = noElements + " users have been deleted";
    userList = []; // Attention: References to userList are not deleted
    res.status(200); // set http status to "Ok"
    //--- prepare and send response ---------------------------------------------
    res.json({ "message": message, "userList": userList }); // return message
});
//# sourceMappingURL=serverCS.js.map