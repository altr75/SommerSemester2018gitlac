var BOOTSTRAP;
(function (BOOTSTRAP) {
    // Constant Value for magic Number 13
    var ENTER_KEY = 13;
    /**********************************************************************************************************************
     * Class "User": represents all data of a user in the userList - class is exported  (needs ES6)                       *
     **********************************************************************************************************************/
    var User = /** @class */ (function () {
        function User() {
        }
        return User;
    }());
    /**********************************************************************************************************************
     * Class "UserList" as Handler and Renderer for all Users.                                                            *
     * - methods for CREATE, UPDATE, DELETE                                                                               *
     * - methods for rendering userList and result                                                                        *
     * UserList is exported                                                                                               *
     **********************************************************************************************************************/
    var UserList = /** @class */ (function () {
        function UserList() {
            // all nessessary DOM-Elements
            this.vornameInput = $("#vornameInput");
            this.nachnameInput = $("#nachnameInput");
            this.usernameInput = $("#usernameInput");
            this.passwortInput = $("#passwortInput");
            this.userTable = $("#userList");
            this.vornameEdit = $("#editVorname");
            this.nachnameEdit = $("#editNachname");
            this.usernameEdit = $("#editUsername");
            this.passwortEdit = $("#editPasswort");
            this.saveBtn = $("#editSaveBtn");
            this.editWindow = $("#editWindow");
            this.resultWindow = $("#resultWindow");
        }
        /**
         * Creates new User with Attributes from Inputfields
         * Only if vorname and nachname are not empty, the User is pushed to userList
         */
        UserList.prototype.createUser = function (event) {
            var _this = this;
            event.preventDefault(); // bit tricky: prevent form from reloading page
            var vorname = this.vornameInput.val().trim();
            var nachname = this.nachnameInput.val().trim();
            var username = this.username.Input.val().trim();
            var passwort = this.passwort.Input.val().trim();
            if (vorname != "" && nachname != "" && username != "" && passwort != "") {
                var data = { "vorname": vorname, "nachname": nachname, "username": username, "passwort": passwort };
                $.ajax({
                    url: 'http://localhost:8080/user',
                    type: 'POST',
                    data: JSON.stringify(data),
                    contentType: 'application/json',
                    dataType: 'json',
                    error: function (jqXHR) {
                        _this.renderResult(jqXHR.responseJSON.message, jqXHR.status);
                    },
                    success: function (data) {
                        _this.renderResult(data.message, 0);
                        _this.renderList(data.userList);
                    }
                });
                this.vornameInput.val(""); // clear input-field
                this.nachnameInput.val(""); // clear input-field
                this.usernameInput.val(""); // clear input-field
                this.passwortInput.val(""); // clear input-field
            }
        };
        UserList.prototype.checkUsername = function (userbame) {
            var _this = this;
            var Usernameexists = false;
            var userlist = User[];
            $.ajax({
                url: 'http://localhost:8080/user/',
                type: 'GET',
                datatype: 'json',
                error: function (jgXHR) {
                    _this.renderResult(jgXHR.responseJSON.message, jgXHR.status);
                },
                success: function (data) {
                    _this.renderResult(data.message, status, 0);
                    _this.renderList(data.userlist);
                    userList = data.userlist;
                    for (var i = 0; i < userlist.length; i++) {
                        console.log(userList[i].userame);
                        if (userList == userList[i].username) {
                            Usernameexists = true;
                        }
                    }
                }
            });
            return Usernameexists;
        };
        /**
         * Changes the value of a User if the Inputfields are not empty and hides the Modal.
         * @param {number} id the specific userID
         */
        UserList.prototype.updateUser = function (id) {
            var _this = this;
            var vorname = this.vornameEdit.val().trim();
            var nachname = this.nachnameEdit.val().trim();
            var username = this.usernameEdit.val().trim();
            var passwort = this.passwortEdit.val().trim();
            if (vorname != "" && nachname != "") {
                var data = { "vorname": vorname, "nachname": nachname };
                $.ajax({
                    url: 'http://localhost:8080/user/' + id,
                    type: 'PUT',
                    data: JSON.stringify(data),
                    contentType: 'application/json',
                    dataType: 'json',
                    error: function (jqXHR) {
                        _this.renderResult(jqXHR.responseJSON.message, jqXHR.status);
                    },
                    success: function (data) {
                        _this.renderResult(data.message, 0);
                        _this.renderList(data.userList);
                    }
                });
                this.editWindow.modal("hide"); // hide modal window
            }
        };
        /**
         * Changes an Array-field to null, so the User is deleted
         * @param {number} id the specific userID
         */
        UserList.prototype.deleteUser = function (id) {
            var _this = this;
            $.ajax({
                url: 'http://localhost:8080/user/' + id,
                type: 'DELETE',
                dataType: 'json',
                error: function (jqXHR) {
                    _this.renderResult(jqXHR.responseJSON.message, jqXHR.status);
                },
                success: function (data) {
                    _this.renderResult(data.message, 0);
                    _this.renderList(data.userList);
                }
            });
        };
        UserList.prototype.deleteallUsers = function () {
            var _this = this;
            event.preventDefault();
            $.ajax({
                url: 'http://localhost:8080/user/',
                type: 'DELETE',
                dataType: 'json',
                error: function (jqXHR) {
                    _this.renderResult(jqXHR.responseJSON.message, jqXHR.status);
                },
                success: function (data) {
                    _this.renderResult(data.message, 0);
                    _this.renderList(data.userList);
                }
            });
        };
        /**
         * Read List of users - needed only during boostrap
         */
        UserList.prototype.readUserlist = function () {
            var _this = this;
            $.ajax({
                url: 'http://localhost:8080/users',
                type: 'GET',
                dataType: 'json',
                error: function (jqXHR) {
                    _this.renderResult(jqXHR.responseJSON.message, jqXHR.status);
                },
                success: function (data) {
                    _this.renderResult(data.message, 0);
                    _this.renderList(data.userList);
                }
            });
        };
        /**
         * Renders a specific User to HTML-Element (Tablerow).
         * Click-Handler are directly integrated (Not pretty, but rare...)
         * Example output:
         * <div class="row bg-white">
         *   <div class='col-12 text-left'>< small>2018-4-17 20:04:46</small> </div>")
         *   <div class="col-5  text-left">Max</div>
         *   <div class="col-5  text-left">Mustermann</div>
         *   <div class="col-1  justify-content-center fa fa-pencil"></div>
         *   <div class="col-1  justify-content-center fa fa-trash" ></div>
         * </div>
         * @param   {User}    user    the specific User
         * @param   {boolean} evenRow  is User an even or an odd one
         * @returns {JQuery}  a table-row representation of user
         */
        UserList.prototype.renderUser = function (user, evenRow) {
            var _this = this;
            //--- alternate background-color
            var div = evenRow ? $("<div class='row bg-white'>") : $("<div class='row bg-light'>");
            //--- append first elements
            div.append($("<div class='col-12 text-left'> <small>" + user.time + "</small> </div>"));
            div.append($("<div class='col-5  text-left'>" + user.vorname + "</div>"));
            div.append($("<div class='col-5  text-left'>" + user.nachname + "</div>"));
            div.append($("<div class='col-5  text-left'>" + user.username + "</div>"));
            div.append($("<div class='col-5  text-left'>" + user.passwort + "</div>"));
            //--- append edit/delete icons, together with its handlers
            div.append($("<div class='col-1  justify-content-center fa fa-pencil text-center'>").on("click", function () {
                _this.renderEdit(user);
            }));
            div.append($("<div class='col-1  justify-content-center fa fa-trash  text-center'>").on("click", function () {
                _this.deleteUser(user.id);
            }));
            //--- return complete table row (to renderList)
            return div;
        };
        /**
         * Renders the List of Users as Bootstrap-Table
         * Example output
         * <div class="row bg-info ">
         *   <div class="col-5 justify-content-center">Vorname</div>
         *   <div class="col-5 justify-content-center">Nachname</div>
         *   <div class="col-5 justify-content-center">Username</div>
         *    <div class="col-5 justify-content-center">Passwort</div>
         *   <div class="col-2"></div>
         * </div>
         * ... users as described in renderUser
         */
        UserList.prototype.renderList = function (userList) {
            //--- clear table content (to fill it afterwards)
            this.userTable.empty();
            //--- set table header and for each user a table-row
            if (userList.length > 0) { // there are users in list -> print header
                this.userTable.append($("\n\t\t\t\t\t<div class=\"row bg-info\">\n\t\t\t\t\t\t<div class=\"col-5 justify-content-center\">Vorname</div>\n\t\t\t\t\t\t<div class=\"col-5 justify-content-center\">Nachname</div>\n\t\t\t\t\t\t<div class=\"col-5 justify-content-center\">Username</div>\n\t\t\t\t\t\t<div class=\"col-5 justify-content-center\">Passwort</div>\n\t\t\t\t\t\t<div class=\"col-2\"></div>\n\t\t\t\t\t</div>\n\t\t\t\t"));
            }
            var evenRow = true; // row-number is even
            for (var _i = 0, userList_1 = userList; _i < userList_1.length; _i++) {
                var user = userList_1[_i];
                if (user != null) { // deleted user (=null) are not displayed
                    this.userTable.append(this.renderUser(user, evenRow));
                    evenRow = !evenRow;
                }
            }
        };
        /**
         * Show the modal editWindow with values of a specific User.
         * @param {string} user the specific user
         */
        UserList.prototype.renderEdit = function (user) {
            var _this = this;
            //--- set values of form fields
            this.editWindow.find("h5.modal-title").text(user.username);
            this.vornameEdit.val(user.vorname);
            this.nachnameEdit.val(user.nachname);
            this.usernameEdit.val(user.username);
            this.passwortEdit.val(user.passwort);
            //--- Edit-Window is used for various users -> remove all handlers
            this.saveBtn.off("click");
            this.vornameEdit.off("keyup");
            this.nachnameEdit.off("keyup");
            this.usernameEdit.off(events, "keyup");
            this.passwortEdit.off(events, "keyup");
            //--- ... and set handlers for current user
            this.saveBtn.on("click", function () {
                _this.updateUser(user.id);
            });
            this.vornameEdit.on("keyup", function (event) {
                if (event.which === ENTER_KEY) {
                    _this.updateUser(user.id);
                }
            });
            this.nachnameEdit.on("keyup", function (event) {
                if (event.which === ENTER_KEY) {
                    _this.updateUser(user.id);
                }
            });
            this.usernameEdit.on("keyup", function (event) {
                if (event.which === ENTER_KEY) {
                    _this.updateUser(user.id);
                }
            });
            this.passwortEdit.on("keyup", function (event) {
                if (event.which === ENTER_KEY) {
                    _this.updateUser(user.id);
                }
            });
            //--- open modal window
            this.editWindow.modal();
        };
        /**
         * Show the resultWindow
         * @param {string} text the message provides by the server
         * @param {string} status the error status
         */
        UserList.prototype.renderResult = function (text, status) {
            this.resultWindow.html(text);
            if (status > 0) { // an error has occured -> set color of result window to orange
                this.resultWindow.removeClass("bg-success");
                this.resultWindow.addClass("bg-danger");
            }
            else { // no error has occured -> set color of result window to green
                this.resultWindow.removeClass("bg-danger");
                this.resultWindow.addClass("bg-success");
            }
        };
        return UserList;
    }());
    /**********************************************************************************************************************
     * Main Event Listener, that waits until DOM is loaded                                                                *
     * - handle click on collapsable items in myContent -> hide all that are shown                                        *
     * - instantiate UserList array                                                                                       *
     * - define handler for clicking add-button or <cr> respectively                                                      *
     **********************************************************************************************************************/
    $(function () {
        //-- instantiate UserList array --------------------------------------------------------------------------------------
        var userList = new UserList();
        //-- initially read UserList (in case of other client already included users) ----------------------------------------
        userList.readUserlist();
        //-- handle click on collapsable items in myContent -> hide all that are shown ---------------------------------------
        // see: https://getbootstrap.com/docs/4.0/components/collapse -> JavaScript
        var contentArea = $('#contentArea'); // consider only elements in contentArea
        contentArea.on('show.bs.collapse', function () {
            contentArea.find('.collapse.show').collapse('hide'); // find shown and hide them
        });
        //--- define handler for clicking add-button or <cr> respectively ----------------------------------------------------
        $("#createBtn").on("click", function () {
            userList.createUser(event);
        });
        $("#vornameInput, #nachnameInput, #usernameInput, #passwortInput").on("keyup", function (event) {
            if (event.which === ENTER_KEY) {
                userList.createUser(event);
            } // only if "enter"-key (=13) is pressed
        });
        $("#deleteBtn").on("click", function () {
            userList.deleteallUsers();
        });
    });
})(BOOTSTRAP || (BOOTSTRAP = {})); // end of namespace BOOTSTRAP
//# sourceMappingURL=clientBS.js.map