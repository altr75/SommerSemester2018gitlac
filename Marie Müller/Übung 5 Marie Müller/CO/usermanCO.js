// Constant Value for magic Number 13
var ENTER_KEY = 13;
/**
 * Class "User" only as struct.
 * All members are public, no more methods.
 */
var User = /** @class */ (function () {
    function User() {
    }
    return User;
}());
/**
 * Class "UserList" as Handler and Renderer for all Users.
 * Methods are CRUD-Operations
 */
var UserList = /** @class */ (function () {
    function UserList() {
        this.userList = [];
        // DOM-Elements
        this.vornameInput = $("#vornameInput");
        this.nachnameInput = $("#nachnameInput");
        this.userTable = $("#userList");
        this.vornameEdit = $("#editVorname");
        this.nachnameEdit = $("#editNachname");
        this.saveBtn = $("#editSaveBtn");
        this.editWindow = $("#editWindow");
    }
    /**
     * Creates new User with Attributes from Inputfields
     * Only if vorname and nachname are not empty, the User is pushed to userList
     */
    UserList.prototype.createUser = function (event) {
        event.preventDefault(); // bit tricky: prevent form from reloading page
        var user = new User();
        user.vorname = this.vornameInput.val().trim();
        user.nachname = this.nachnameInput.val().trim();
        user.id = this.userList.length;
        if (user.vorname != "" && user.nachname != "") {
            this.userList.push(user); // append user to array
            this.vornameInput.val(""); // clear input-field
            this.nachnameInput.val(""); // clear input-field
            this.renderList(); // render list with new user
        }
    };
    /**
     * Changes the value of a User if the Inputfields are not empty and hides the Modal.
     * @param {number} id the specific userID
     */
    UserList.prototype.updateUser = function (id) {
        var user = this.userList[id];
        var vorname = this.vornameEdit.val().trim();
        var nachname = this.nachnameEdit.val().trim();
        if (vorname != "" && nachname != "") {
            user.vorname = vorname;
            user.nachname = nachname;
            this.renderList();
            this.editWindow.modal("hide"); // hide modal window
        }
    };
    /**
     * Changes an Array-field to null, so the User is deleted
     * @param {number} id the specific userID
     */
    UserList.prototype.deleteUser = function (id) {
        this.userList[id] = null;
        this.renderList();
    };
    /**
     * Renders a specific User to HTML-Element (Tablerow).
     * Click-Handler are directly integrated (Not pretty, but rare...)
     * Example output:
     * <div class="row bg-white">
     *   <div class="col-5">Max</div>
     *   <div class="col-5">Mustermann</div>
     *   <div class="col-1 justify-content-center fa fa-pencil"></div>
     *   <div class="col-1 justify-content-center fa fa-trash" ></div>
     * </div>
     * @param   {User}    user    the specific User
     * @param   {boolean} evenRow  is User an even or an odd one
     * @returns {JQuery}  a table-row representation of user
     */
    UserList.prototype.renderUser = function (user, evenRow) {
        var _this = this;
        //--- alternate background-color
        var div = evenRow ? $("<div class='row bg-white'>") : $("<div class='row bg-light'>");
        //--- append first elements
        div.append($("<div class='col-5'>" + user.vorname + "</div>"));
        div.append($("<div class='col-5'>" + user.nachname + "</div>"));
        //--- append edit/delete icons, together with its handlers
        div.append($("<div class='col-1 justify-content-center fa fa-pencil text-center'>").on("click", function () {
            _this.renderEdit(user.id);
        }));
        div.append($("<div class='col-1 justify-content-center fa fa-trash  text-center'>").on("click", function () {
            _this.deleteUser(user.id);
        }));
        //--- return complete table row (to renderList)
        return div;
    };
    /**
     * Renders the List of Users as HTML-Table
     * Example:
     * <div class="row bg-info ">
     *   <div class="col-5 justify-content-center">Vorname</div>
     *   <div class="col-5 justify-content-center">Nachname</div>
     *   <div class="col-2"></div>
     * </div>
     * <div class="row bg-white">
     *   <div class="col-5">Max</div>
     *   <div class="col-5">Mustermann</div>
     *   <div class="col-1 justify-content-center fa fa-pencil"></div>
     *   <div class="col-1 justify-content-center-center fa fa-trash" ></div>
     * </div>
     * <div class="row bg-light">
     *   <div class="col-5">Sabine</div>
     *   <div class="col-5">Musterfrau</div>
     *   <div class="col-1 justify-content-center fa fa-pencil"></div>
     *   <div class="col-1 justify-content-center fa fa-trash" ></div>
     * </div>
     */
    UserList.prototype.renderList = function () {
        //--- clear table content (to fill it afterwards)
        this.userTable.empty();
        //--- set table header and for each user a table-row
        var headerNeeded = true; // table-header still needed
        var evenRow = true; // row-number is even
        for (var _i = 0, _a = this.userList; _i < _a.length; _i++) {
            var user = _a[_i];
            if (user != null) {
                if (headerNeeded) { // append table-header with first User
                    this.userTable.append($("\n\t\t\t\t\t\t<div class=\"row bg-info \">\n\t\t\t\t\t\t\t<div class=\"col-5 justify-content-center\">Vorname</div>\n\t\t\t\t\t\t\t<div class=\"col-5 justify-content-center\">Nachname</div>\n\t\t\t\t\t\t\t<div class=\"col-2\"></div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t"));
                    headerNeeded = false; // table-header has been added
                }
                this.userTable.append(this.renderUser(user, evenRow));
                evenRow = !evenRow;
            }
        }
    };
    /**
     * Show the Modal with Values of a specific User.
     * Important: Before registering the Update-Handler, remove the old ones!
     * @param {number} id the specific userID
     */
    UserList.prototype.renderEdit = function (id) {
        var _this = this;
        var user = this.userList[id];
        //--- set values of form fields
        this.editWindow.find("h5.modal-title").text(user.vorname + " " + user.nachname);
        this.vornameEdit.val(user.vorname);
        this.nachnameEdit.val(user.nachname);
        //--- Edit-Window is used for various users -> remove handlers
        this.saveBtn.off("click");
        this.vornameEdit.off("keyup");
        this.nachnameEdit.off("keyup");
        //--- ... and newly set handlers for current user
        this.saveBtn.on("click", function () { _this.updateUser(id); });
        this.vornameEdit.on("keyup", function (event) {
            if (event.which === ENTER_KEY) {
                _this.updateUser(id);
            }
        });
        this.nachnameEdit.on("keyup", function (event) {
            if (event.which === ENTER_KEY) {
                _this.updateUser(id);
            }
        });
        //--- open modal window
        this.editWindow.modal();
    };
    return UserList;
}());
/**
 * Waits for DOM-Ready-Event.
 * Creates a new UserList and registers the Create-Handler.
 */
$(function () {
    //-- handle click on collapsable items in myContent -> hide all that are shown
    // see: https://getbootstrap.com/docs/4.0/components/collapse -> JavaScript
    var contentArea = $('#contentArea'); // consider only elements in contentArea
    contentArea.on('show.bs.collapse', function () {
        contentArea.find('.collapse.show').collapse('hide'); // find shown and hide them
    });
    //-- instantiate UserList array
    var userList = new UserList();
    //--- define handlers for clicking button and <cr> respectively
    $("#createBtn").on("click", function () { userList.createUser(event); });
    $("#vornameInput, #nachnameInput").on("keyup", function (event) {
        if (event.which === ENTER_KEY) {
            userList.createUser(event);
        }
    });
});
//# sourceMappingURL=usermanCO.js.map