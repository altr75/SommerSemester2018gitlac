import {Request, Response} from "express";
import express = require("express");
import bodyParser = require("body-parser");

let router = express();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: true}));
router.listen(8080, () => {
    console.log("Gestartet: http://localhost:8080/raten/5");
    console.log("Gestartet: http://localhost:8080/cheat/:password");
    console.log("Gestartet: http://localhost:8080/reset");
    console.log("Gestartet: http://localhost:8080/set")
});

let zahl: number = 5;

function random(): number {
    let min = 1;
    let max = 10;
    return Math.floor(Math.random() * (max - min)) + min;
}

// Aufgabe 1
// Beim Aufruf der Route mit einer konkreten Zahl, soll ein entsprechender Text als JSON-Objekt zurückgesendet werden.

router.get("/raten/:zahl", function (req: Request, res: Response) {
    if (zahl < req.params ["zahl"]) {
        res.json({antwort: "Kleiner!"})
    }
    if (zahl > req.params ["zahl"]) {
        res.json({antwort: "Größer!"})
    }
    if (zahl == req.params ["zahl"]) {
        res.json({antwort: "Richtig!"})
    }
    else {
        res.json({antwort: "Fehler!"})
    }
});

// Diese Route soll die gesuchte Zahl zurückschicken. Sie dient zum Schummeln oder zur Fehlersuche.
// Das Übertragen darf nur stattfinden, wenn das übermittelte Passwort korrekt ist.
// Denken Sie sich dazu ein einfaches Passwort aus, welches sie im Server als Variable anlegen.

let password: string = "swordfish";

router.get("/cheat/:password", function (req: Request, res: Response) {
    if (password == req.params ["password"]) {
        res.json({"Zahl": zahl})
    }
    else {
        res.json({"Antwort": "Falsches Passwort"})
    }
});

// Diese Router soll erneut eine zufällige Zahl setzen.

router.get("/reset", function (req: Request, res: Response) {
    zahl = random();
    res.json({"Antwort": "NEW PASSWORD IS" + " " + zahl })

});

// Aufgabe 2

router.get("/set/:newnumber", function (req: Request, res: Response) {
    zahl = req.params["newnumber"];
    res.json({ zahl })

});


