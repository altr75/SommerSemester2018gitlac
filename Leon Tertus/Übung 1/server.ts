import {Request, Response} from "express";
import express = require("express");
import bodyParser = require("body-parser");

let router = express();
router.use(bodyParser.json());
router.listen(8080, () => {
  console.log("Gestartet!");
  console.log("Aufrufbar sollten sein: ");
  console.log("  http://localhost:8080/name");
  console.log("  http://localhost:8080/htmlfile");
  console.log("  http://localhost:8080/gauss");
  console.log("  http://localhost:8080/liste");
  console.log("  http://localhost:8080/object");
});

// Aufgabe 1.1
router.get("/name", function(req: Request, res:Response) {
  res.json({ vorname: "Leon", nachname:"Tertus"});
    });

// Aufgabe 1.2
router.use("/htmlfile", express.static(__dirname + "/names.html"));

// Aufgabe 1.3
router.get("/gauss", (req, res) => {
    let ende: number = 101;
    let i: number;
    let summe: number = 0;

    for (i = 1; i < ende; i++) {
        summe = summe + i;
    }
    res.json({Ergebnis: summe,})
});

// Aufgabe 1.4
router.get("/liste", (req, res) => {
    let x: number;
    let erg: number [] = [];

    for (x = 0; x < 100; x++) {
        erg.push(x)
    }
    res.json({Ergebnis: erg});
});

// Aufgabe 1.5
router.get("/object", (req, res) => {

    class Person {
        firstname: string;
        lastname: string;

        constructor(firstname:string, lastname: string) {
            this.firstname = firstname;
            this.lastname = lastname;
        }
        getFullName(): string {
            return (this.firstname + " "+ this.lastname)
        }
    }
    let name1 = new Person ("Leon", "Tertus");
    res.json({"Person": name1.getFullName() });
});