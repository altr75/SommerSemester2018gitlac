"use strict";
exports.__esModule = true;
var express = require("express");
var bodyParser = require("body-parser");
var router = express();
router.use(bodyParser.json());
router.listen(8080, function () {
    console.log("Gestartet!");
    console.log("Aufrufbar sollten sein: ");
    console.log("  http://localhost:8080/name");
    console.log("  http://localhost:8080/htmlfile");
    console.log("  http://localhost:8080/gauss");
    console.log("  http://localhost:8080/liste");
    console.log("  http://localhost:8080/object");
});
// Aufgabe 1.1
router.get("/name", function (req, res) {
    res.json({ vorname: "Leon", nachname: "Tertus" });
});
// Aufgabe 1.2
router.use("/htmlfile", express.static(__dirname + "/names.html"));
// Aufgabe 1.3
router.get("/gauss", function (req, res) {
    var ende = 101;
    var i;
    var summe = 0;
    for (i = 1; i < ende; i++) {
        summe = summe + i;
    }
    res.json({ Ergebnis: summe });
});
// Aufgabe 1.4
router.get("/liste", function (req, res) {
    var x;
    var erg = [];
    for (x = 0; x < 100; x++) {
        erg.push(x);
    }
    res.json({ Ergebnis: erg });
});
// Aufgabe 1.5
router.get("/object", function (req, res) {
    var Person = /** @class */ (function () {
        function Person(firstname, lastname) {
            this.firstname = firstname;
            this.lastname = lastname;
        }
        Person.prototype.getFullName = function () {
            return (this.firstname + " " + this.lastname);
        };
        return Person;
    }());
    var name1 = new Person("Leon", "Tertus");
    res.json({ "Person": name1.getFullName() });
});
//# sourceMappingURL=server.js.map