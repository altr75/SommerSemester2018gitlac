-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Erstellungszeit: 12. Mrz 2018 um 14:21
-- Server-Version: 10.1.28-MariaDB
-- PHP-Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `userman`
--
CREATE DATABASE IF NOT EXISTS `userman` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `userman`;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `grouplist`
--

CREATE TABLE `grouplist` (
  `id` int(11) NOT NULL,
  `time` varchar(32) NOT NULL,
  `groupname` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `grouplist`
--

INSERT INTO `grouplist` (`id`, `time`, `groupname`) VALUES
(1, '2017-9-11 11:27:38', 'administrator'),
(2, '2017-9-11 11:27:38', 'manager'),
(3, '2017-9-11 11:27:38', 'publisher'),
(4, '2017-9-11 11:27:38', 'editor'),
(5, '2017-9-11 11:27:38', 'author'),
(6, '2017-9-11 11:27:38', 'registered');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `mapUsersGroups`
--

CREATE TABLE `mapUsersGroups` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `groupid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `mapUsersGroups`
--

INSERT INTO `mapUsersGroups` (`id`, `userid`, `groupid`) VALUES
(1, 10, 1),
(2, 10, 2),
(3, 10, 3),
(4, 10, 4),
(5, 10, 5),
(6, 10, 6),
(7, 22, 5),
(8, 23, 5),
(9, 24, 5),
(10, 25, 5),
(11, 26, 5);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `userlist`
--

CREATE TABLE `userlist` (
  `id` int(11) NOT NULL,
  `time` varchar(32) NOT NULL,
  `vorname` varchar(128) NOT NULL,
  `nachname` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `userlist`
--

INSERT INTO `userlist` (`id`, `time`, `username`, `password`, `vorname`, `nachname`) VALUES
(10, '2017-9-11 11:27:38', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Peter', 'Kneisel'),
(22, '2017-9-30 11:27:31', 'Dennis', '4f121df643601c11cf5fc929b92a7766', 'Dennis', 'Priefer'),
(23, '2018-3-12 11:27:38', 'Wolf', '37d1b676a2c236fd6b56a858bc0547fa', 'Wolf', 'Rost'),
(24, '2018-3-12 11:27:49', 'Kevin', '038427f7ace86381c9b814a5319583dd', 'Kevin', 'Linne'),
(25, '2018-3-12 11:28:01', 'Samuel', '4ade21dd86fdd88989123a399c4ab1c0', 'Samuel', 'Schepp'),
(26, '2018-3-12 11:28:23', 'Dieudonne', 'fd0f017babc7cfd2ae9446163a24161b', 'Dieudonne', 'Timma');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `grouplist`
--
ALTER TABLE `grouplist`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indizes für die Tabelle `mapUsersGroups`
--
ALTER TABLE `mapUsersGroups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userid` (`userid`),
  ADD KEY `groupid` (`groupid`);

--
-- Indizes für die Tabelle `userlist`
--
ALTER TABLE `userlist`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `id` (`id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `grouplist`
--
ALTER TABLE `grouplist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT für Tabelle `mapUsersGroups`
--
ALTER TABLE `mapUsersGroups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT für Tabelle `userlist`
--
ALTER TABLE `userlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `mapUsersGroups`
--
ALTER TABLE `mapUsersGroups`
  ADD CONSTRAINT `mapusersgroups_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `userlist` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `mapusersgroups_ibfk_2` FOREIGN KEY (`groupid`) REFERENCES `grouplist` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
